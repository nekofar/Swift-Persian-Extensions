# Swift-Persian-Extensions (v0.9.3)
This library includes Persian extensions for NSDate, String, Int &amp;...

## Installation
Just copy the `PersianExtensions` folder into your project !

## API Usage

### NSDate Extension

##### .persianDate()
---
```sh
myNSDate.persianDate()
```
- Something like: خروجی: ۱۶ فروردین ۱۳۷۵

##### .persianTime()
---
```sh
myNSDate.persianTime()
```
- Something like: ۱۲:۰۰

##### .smartDate()
---
```sh
myNSDate.smartDate()
```
- Something like:

خروجی برای امروز: ۲ ساعت قبل    و    خروجی برای روز های قبل: ۱۶ فروردین ۱۳۷۵

##### .persianDateTime()
---
```sh
myNSDate.persianDateTime()
```
- Something like : دوشنبه ۱ اسفند ۱۳۹۴ . ۲۳:۲۳

##### .numberOfDaysUntilDateTime(anorherNSDate)
---
```sh
myNSDate.numberOfDaysUntilDateTime(anorherNSDate)
```
- Something like : 200 (it's in day)

##### .dayOfWeek()
---
```sh
myNSDate.dayOfWeek()
```
- A number between 1 to 7 (1 is sunday)

##### .dayOfWeekInJalaali()
---
```sh
myNSDate.dayOfWeekInJalaali()
```
- A number between 1 to 7 (1 is sturday)

##### .dayOfWeekInPersian()
---
```sh
myNSDate.dayOfWeekInPersian()
```
- Something like: شنبه

##### .timeAgo()
---
```sh
myNSDate.timeAgo()
```
- Something like:

خروجی: ۲ روز قبل

خروجی: ۲ ساعت قبل

##### .numberOfDaysUntilDateTime(anorherNSDate)
---
```sh
myNSDate.numberOfDaysUntilDateTime(anorherNSDate)
```
- Something like : 200 (it's in day)

##### .isInSameDayAs(anorherNSDate)
---
```sh
myNSDate.isInSameDayAs(someNSDate)
```
- True or False

##### .isToday()
---
```sh
myNSDate.isToday()
```
- True or False

### String Extension

##### .persianDigits()
---
```sh
myString.persianDigits()
```

- Input: میخواهیم عدد 24 را فارسی کنیم
- Output: میخواهیم عدد ۲۴ را فارسی کنیم

### Int Extension

##### .persianDigits()
---
```sh
myString.persianDigits()
```

- Something like: ۲۴

##### .persianTimeDigits()
---
```sh
myString.persianTimeDigits()
```

- Input: 5
- Output: ۰۵

- Input: 30 
- Output: ۳۰