//
//  String+PersianExtension.swift
//  Persian Extensions
//
//  Created by Sina Khalili on 5/13/16.
//  Copyright © 2016 Sina Khalili. All rights reserved.
//

import Foundation

extension String {
    
    mutating func persianDigits() -> String {
        self = self.stringByReplacingOccurrencesOfString("0", withString: "۰")
        self = self.stringByReplacingOccurrencesOfString("1", withString: "۱")
        self = self.stringByReplacingOccurrencesOfString("2", withString: "۲")
        self = self.stringByReplacingOccurrencesOfString("3", withString: "۳")
        self = self.stringByReplacingOccurrencesOfString("4", withString: "۴")
        self = self.stringByReplacingOccurrencesOfString("5", withString: "۵")
        self = self.stringByReplacingOccurrencesOfString("6", withString: "۶")
        self = self.stringByReplacingOccurrencesOfString("7", withString: "۷")
        self = self.stringByReplacingOccurrencesOfString("8", withString: "۸")
        self = self.stringByReplacingOccurrencesOfString("9", withString: "۹")
        return self
    }
    
}