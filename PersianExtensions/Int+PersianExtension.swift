//
//  Int+PersianExtension.swift
//  Persian Extensions
//
//  Created by Sina Khalili on 5/13/16.
//  Copyright © 2016 Sina Khalili. All rights reserved.
//

import Foundation

extension Int {
    
    /**
     Converts the int digits to persian characters
     
     - returns: String of number with persian characters
     */
    func persianDigits() -> String {
        let number = NSNumber(integer: self)
        let formatter = NSNumberFormatter()
        let locale = NSLocale(localeIdentifier: "fa")
        formatter.locale = locale
        formatter.numberStyle = .NoStyle
        return formatter.stringFromNumber(number)!
    }
    
    /**
     Converts the integer to h,m,s format to show (+persian digits)
     
     - returns: String in h,m,s format to show
     */
    func persianTimeDigits() -> String {
        if self >= 10 {
            return persianDigits()
        } else {
            return "۰\(persianDigits())"
        }
    }
    
}
